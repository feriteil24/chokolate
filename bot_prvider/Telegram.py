import telebot
import argparse
import sys
import argparse
from django.db import models
from bot_switcher.models import Provider, Chat

parser = argparse.ArgumentParser()
parser.add_argument('-t')
args = parser.parse_args()

bot = telebot.TeleBot(args.t)

@bot.message_handler(commands=['start', 'stop'])
def start_message(message):
    bot.send_message(message.chat.id, 'нан')

@bot.message_handler(content_types=['text'])
def send_text(message):
    if message.text.lower() == 'привет':
        bot.send_message(message.chat.id, 'Привет, мой создатель')
    elif message.text.lower() == 'пока':
        bot.send_message(message.chat.id, 'Прощай, создатель')
        sys.exit()

bot.polling()
