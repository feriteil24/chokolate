import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'chokolate.settings')

app = Celery('chokolate')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()