from django.apps import AppConfig


class BotSwitcherConfig(AppConfig):
    name = 'bot_switcher'
