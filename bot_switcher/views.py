from django.shortcuts import render
from .tasks import execute_bot


def about(request):
    execute_bot.delay()
    return render(request, 'about.html')

