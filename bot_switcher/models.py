from django.db import models

# Create your models here.


class Provider(models.Model):
    type = models.CharField(max_length=255)
    module = models.CharField(max_length=255)

    def __str__(self):
        return self.type


class Bot(models.Model):
    name = models.CharField(max_length=255)
    token = models.CharField(max_length=255)
    config_id = models.IntegerField()
    provider_id = models.ForeignKey(Provider,on_delete=models.CASCADE, related_name='bots')

    def __str__(self):
        return self.name


class Chat(models.Model):
    id_chat = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    bot_id = models.ForeignKey(Bot,on_delete=models.CASCADE, related_name='chats')
    users_id = models.IntegerField()

    def __str__(self):
        return self.name