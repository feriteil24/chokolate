from chokolate.celery import app

from .service import bot


@app.task
def execute_bot():
    bot()