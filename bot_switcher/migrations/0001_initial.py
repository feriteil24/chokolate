# Generated by Django 3.0.1 on 2020-07-04 07:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('token', models.CharField(max_length=255)),
                ('config_id', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Provider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=255)),
                ('module', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_chat', models.CharField(max_length=255)),
                ('name', models.CharField(max_length=255)),
                ('users_id', models.IntegerField()),
                ('bot_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='chats', to='bot_switcher.Bot')),
            ],
        ),
        migrations.AddField(
            model_name='bot',
            name='provider_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bots', to='bot_switcher.Provider'),
        ),
    ]
